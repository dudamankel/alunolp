unit Unit3;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.MySQL,
  FireDAC.Phys.MySQLDef, FireDAC.VCLUI.Wait, FireDAC.Stan.Param, FireDAC.DatS,
  FireDAC.DApt.Intf, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client;

type
  TDataModule3 = class(TDataModule)
    FDConnection1: TFDConnection;
    FDQueryEmpresas: TFDQuery;
    FDQueryGrupo: TFDQuery;
    DataSourceEmpresas: TDataSource;
    DataSourceGrupo: TDataSource;
    FDPhysMySQLDriverLink1: TFDPhysMySQLDriverLink;
    procedure DataModule(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DataModule3: TDataModule3;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

procedure TDataModule3.DataModule(Sender: TObject);
begin
FDQueryEmpresas.Open();
FDQueryGrupo.Open();
end;

end.
