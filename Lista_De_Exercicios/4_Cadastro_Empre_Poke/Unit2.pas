unit Unit2;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.DBCtrls;

type
  TForm2 = class(TForm)
    Panel1: TPanel;
    btn_cancelar: TButton;
    btn_salvar: TButton;
    lb_nome: TLabel;
    lb_grupo: TLabel;
    ed_nome: TEdit;
    DBLookupComboBox1: TDBLookupComboBox;
    lb_nivel: TLabel;
    ed_nivel: TEdit;
    procedure btn_salvarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form2: TForm2;

implementation

{$R *.dfm}

uses Unit1, Unit3;

procedure TForm2.btn_salvarClick(Sender: TObject);
begin
DataModule3.FDQueryEmpresas.Post;
Form2.Close;
end;

end.
