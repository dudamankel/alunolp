object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'RPG'
  ClientHeight = 453
  ClientWidth = 490
  Color = clCream
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 24
    Top = 23
    Width = 458
    Height = 402
    Color = clGradientInactiveCaption
    ParentBackground = False
    TabOrder = 0
    object LBoxPerso: TListBox
      Left = 208
      Top = 32
      Width = 241
      Height = 353
      ItemHeight = 13
      TabOrder = 0
      OnClick = LBoxPersoClick
    end
    object Atualizar: TButton
      Left = 40
      Top = 88
      Width = 121
      Height = 33
      Caption = 'Atualizar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Vineta BT'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnClick = AtualizarClick
    end
    object Button1: TButton
      Left = 40
      Top = 368
      Width = 75
      Height = 25
      Caption = 'Next'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Vineta BT'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      OnClick = Button1Click
    end
  end
  object NetHTTPClient1: TNetHTTPClient
    Asynchronous = False
    ConnectionTimeout = 60000
    ResponseTimeout = 60000
    AllowCookies = True
    HandleRedirects = True
    UserAgent = 'Embarcadero URI Client/1.0'
    Left = 32
    Top = 39
  end
end
