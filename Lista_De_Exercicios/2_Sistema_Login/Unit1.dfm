object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 434
  ClientWidth = 377
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 120
    Top = 16
    Width = 249
    Height = 18
    Alignment = taCenter
    Caption = 'TELA DE LOGIN'
    Font.Charset = OEM_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Terminal'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Panel1: TPanel
    Left = 56
    Top = 66
    Width = 257
    Height = 303
    TabOrder = 0
    object ed_usuario: TEdit
      Left = 64
      Top = 56
      Width = 145
      Height = 21
      TabOrder = 0
    end
    object ed_senha: TEdit
      Left = 64
      Top = 112
      Width = 145
      Height = 21
      PasswordChar = '*'
      TabOrder = 1
    end
    object btn_login: TButton
      Left = 96
      Top = 200
      Width = 75
      Height = 25
      Caption = 'Login'
      TabOrder = 2
      OnClick = btn_loginClick
    end
  end
  object NetHTTPClient1: TNetHTTPClient
    Asynchronous = False
    ConnectionTimeout = 60000
    ResponseTimeout = 60000
    AllowCookies = True
    HandleRedirects = True
    UserAgent = 'Embarcadero URI Client/1.0'
    Left = 328
    Top = 320
  end
end
