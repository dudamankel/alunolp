unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ImgList, ExtCtrls;

type
  TForm1 = class(TForm)
    lb_esportes: TLabel;
    fundoBox: TGroupBox;
    imagemBox: TGroupBox;
    lb_tipo: TLabel;
    ed_tipo: TEdit;
    lb_num: TLabel;
    ed_num: TEdit;
    btn_enviar: TButton;
    btn_remover: TButton;
    btn_atualizar: TButton;
    image: TImage;
    esportesBox: TListBox;
    btn_salvar: TButton;
    lb_descricao: TLabel;
    ed_descricao: TEdit;
    procedure btn_enviarClick(Sender: TObject);
    procedure btn_removerClick(Sender: TObject);
    procedure btn_salvarClick(Sender: TObject);
    procedure ed_tipoChange(Sender: TObject);
    procedure ed_numChange(Sender: TObject);
    procedure ed_descricaoChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.btn_enviarClick(Sender: TObject);
begin
esportesBox.Items.Add(ed_tipo.Text);
esportesBox.Items.Add(ed_num.Text);
esportesBox.Items.Add(ed_descricao.Text);
end;


procedure TForm1.btn_removerClick(Sender: TObject);
begin
esportesBox.DeleteSelected;
if MessageDlg('Deseja excluir todos os itens?', mtConfirmation,
  [mbYes, mbNo], 0) = mrYes then
begin
    esportesBox.Items.Clear;
    ed_tipo.Clear;
    ed_num.Clear;
    ed_descricao.Clear;
end

end;
procedure TForm1.btn_salvarClick(Sender: TObject);
begin

 esportesBox.Items.SaveToFile('C:\Users\Duda\Documents\CEDUP\2019\LP\ProjetoTrimestralLP01\salvar.txt') ;
 ShowMessage('Esportes gravados com sucesso em "salvar.txt".');

end;

procedure TForm1.ed_tipoChange(Sender: TObject);
begin
if (ed_tipo.Text) = '' then
begin
   ShowMessage('Campo Obrigatório!')
end
end;
procedure TForm1.ed_numChange(Sender: TObject);
begin
if (ed_num.Text) = '' then
  begin
     ShowMessage('Campo obrigatório!')
  end
end;

procedure TForm1.ed_descricaoChange(Sender: TObject);
begin
if (ed_descricao.Text) = '' then
  begin
     ShowMessage('Campo obrigatório!')
  end
end;

end.

